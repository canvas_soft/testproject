#define USE_PURCHASE

using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Linq;

#if USE_PURCHASE
using UnityEngine.Purchasing;

namespace Client.Framework.Purchases
{
	// IStoreListener を継承うることでUnity Purchasing からメッセージを取得できる
	public class Purchaser : MonoBehaviour, IStoreListener
	{
		private IStoreController m_StoreController;              // Purchasing システムの参照
		private IExtensionProvider m_StoreExtensionProvider;     // 拡張した場合のPurchasing サブシステムの参照

		// コールバック
		private Action onInitalizeSuccess = delegate {};
		private Action<string> onInitalizeFailed = delegate {};
		
		private Action<string> onBuyFailed = delegate {};
		private Action<string> onBuySuccess = delegate {};

		// 商品情報
		public List<ProductData> productDataList = new List<ProductData>();
		private List<string> productKeyList = new List<string>(); 

		//-----------------------------------------------------------
		// 商品の登録
		//-----------------------------------------------------------
		// 実際に購入ボタンを押した時に初めてコールする 
		// 失敗した場合は InitializationFailureReason の文字列が返る
		//-----------------------------------------------------------
		public void Initialize(
			List<string> nonConsumableItemList,
			List<string> consumableItemList,
			Action onSuccess,
			Action<string> onFailed) 
		{
			if (IsInitialized())return;
	
			this.onInitalizeSuccess = onSuccess;
			this.onInitalizeFailed = onFailed;

			#if UNITY_EDITOR			
			var module = StandardPurchasingModule.Instance();

			// エディターデバッグUI表示
			module.useMockBillingSystem = true;

			// フェイクStoreは以下のことをサポートしている： UIなし（いつも成功する）、基本的なUI（購入が成功/失敗）
			// 開発者向けUI（初期化、購入、失敗時の設定）。　これらは FakeStoreUIMode の Enum値と対応している
			module.useFakeStoreUIMode = FakeStoreUIMode.StandardUser;
			#endif
			
			// Create a builder, first passing in a suite of Unity provided stores.
			// builder を生成し、Unity謹製ストアの最初の部分をパスする
			Debug.Log("ConfigurationBuilder.Instance");
			var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
	
			//---------------------------------------------------
			// リストア商品の追加
			//---------------------------------------------------
			foreach(string item in nonConsumableItemList)
			{
				Debug.Log ("AddProduct NonConsumable:" + item);
				productKeyList.Add(item);
				builder.AddProduct(item, ProductType.NonConsumable);
				//builder.AddProduct(item, ProductType.NonConsumable,	new IDs() {	{ string.Format("{0}.{1}", GameConfig.BundleId, item), AppleAppStore.Name, GooglePlay.Name},});
			}
	
			//---------------------------------------------------
			// 消費型商品の追加
			//---------------------------------------------------
			foreach(string item in consumableItemList)
			{
				Debug.Log ("AddProduct Consumable:" + item);
				productKeyList.Add(item);

				// com.xxx.yyy.Coin0のままやりとりをする場合
				builder.AddProduct(item, ProductType.Consumable);

				// bundleIdを省きたい場合
				//builder.AddProduct(item, ProductType.Consumable, new IDs() {{ string.Format("{0}.{1}", GameConfig.BundleId, item), AppleAppStore.Name, GooglePlay.Name},});
			}
	
			// ここまでで設定したものとこのクラスのインスタンスを引数として、非同期呼び出しで設定して開始。
			// その後、OnInitialized か OnInitializeFailedが呼び出されるはず。
			Debug.Log ("UnityPurchasing.Initialize");
			UnityPurchasing.Initialize(this, builder);
		}
	
		//-----------------------------------------------------------
		// 
		//-----------------------------------------------------------
		public bool IsInitialized()
		{
			// Only say we are initialized if both the Purchasing references are set.
			// 二つのPurchasing の参照が設定されていれば、初期設定されていると言える
			return m_StoreController != null && m_StoreExtensionProvider != null;
		}
	
		public Product GetProduct(string key)
		{
			return m_StoreController.products.WithID(key);
		}

		//-----------------------------------------------------------
		// 購入処理
		//-----------------------------------------------------------
		public bool BuyProductID(string productId, Action<string> onSuccess, Action<string> onFailed)
		{
			// ストアの中で例外が出れば、try catch を使って、ここのロジックでキャッチ
			try
			{
				this.onBuySuccess = onSuccess;
				this.onBuyFailed = onFailed;
				
				// 購入が初期化されていれば
				if (!IsInitialized())
				{
					Debug.Log("BuyProductID FAIL. Not initialized.");
					return false;
				}
				
				// 汎用製品IDと、購入システムの製品群からProduct参照を取得する。
				Product product = m_StoreController.products.WithID(productId);
	
				// もしデバイスのストアで製品が見つかったら、販売される用意が出来たということになる
				if (product == null || !product.availableToPurchase)
				{
					Debug.Log ("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
					return false;
				}
	
				// 製品を買う。非同期で ProcessPurchase か OnPurchaseFailed の呼び出しの反応がある
				Debug.Log (string.Format("Purchasing product asychronously: '{0}' - '{1}'", product.definition.id, product.definition.storeSpecificId));
				m_StoreController.InitiatePurchase(product);
			}
			// 例外ハンドリング
			catch (Exception e)
			{
				// ... by reporting any unexpected exception for later diagnosis.
				// ... あとで分析で例外をレポート
				Debug.Log ("BuyProductID: FAIL. Exception during purchase. " + e);
				return false;
			}

			return true;
		}
	
		//-----------------------------------------------------------
		// 以前買ったことがある商品のリストア処理
		//-----------------------------------------------------------
		// いくつかのプラットフォームは自動的にリストアする。
		// AppleはIAPの中で明示的に購入リストアを要求している
		//-----------------------------------------------------------
		public void RestorePurchases(Action<bool> onFinished)
		{
			// If Purchasing has not yet been set up ...
			if (!IsInitialized())
			{
				// ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
				Debug.Log("RestorePurchases FAIL. Not initialized.");
				return;
			}

			#if UNITY_EDITOR
			onFinished(false);
			return;
			#endif
			
			// If we are running on an Apple device ... 
			// Apple なら
			if (Application.platform == RuntimePlatform.IPhonePlayer || 
				Application.platform == RuntimePlatform.OSXPlayer)
			{
				// ... begin restoring purchases
				// リストア開始
				Debug.Log("RestorePurchases started ...");

				// Fetch the Apple store-specific subsystem.
				// Apple特有のサブシステムを取ってくる
				var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
				// Begin the asynchronous process of restoring purchases. Expect a confirmation response in the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
				// 購入リストアの非同期処理を開始。下記のAction<bool> の中で、確認プロセスが呼び出される。もし以前買っているのならProcessPurchaseが呼び出される
				apple.RestoreTransactions((result) => 
				{
					// The first phase of restoration. If no more responses are received on ProcessPurchase then no purchases are available to be restored.
					// リストア処理の最初の	フェーズ。もし、ProcessPurchaseで反応がないのなら、リストアするべき購入はなかったということ		
					Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
					onFinished(result);
				});
			}
			// Otherwise ...
			else
			{
				// We are not running on an Apple device. No work is necessary to restore purchases.
				Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
			}
		}
	
	
		//===========================================================
		// 
		// コールバック
		//
		//===========================================================
	
		//===========================================================
		// 初期化が完了
		//===========================================================
		public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
		{
			// Purchasing は初期化に成功した。Purchasing 参照を取っておく
			Debug.Log("OnInitialized: PASS");
	
			// 全体のPurchasingシステム。このアプリケーションの製品を構成している。
			m_StoreController = controller;
	
			// ストア特有のサブシステム。デバイス特有のストアへのアクセス等ができる。
			m_StoreExtensionProvider = extensions;
			
			// 値段を取得
			foreach(string key in productKeyList)
			{
				Product product = GetProduct(key);
				if(product == null)continue;

				Debug.Log ("product:" + key);

				productDataList.Add(
					new ProductData(
						key,
						product.metadata.isoCurrencyCode,
						product.metadata.localizedDescription,
						product.metadata.localizedPrice.ToString(),
						product.metadata.localizedPriceString,
						product.metadata.localizedTitle)
				);
			}

			onInitalizeSuccess();
		}
	
		//===========================================================
		// 初期化失敗
		//===========================================================
		public void OnInitializeFailed(InitializationFailureReason error)
		{
			Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
			onInitalizeFailed(error.ToString());
		}
	
		//===========================================================
		// 購入完了
		//===========================================================
		public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args) 
		{
			// エディタでは、「Buy」と押すと、すぐにコンソールに「ProcessPurchase: PASS」と表示される。
			// 本来デバイスでは、ポップアップで購入確認ウィンドウが出る。
	
			// セーブ完了を待った方がよいので本当はコールバックの方がよいか？？？
			Debug.Log("ProcessPurchase:"+args.purchasedProduct.definition.id);
			onBuySuccess(args.purchasedProduct.definition.id);

			// これを返すことで購入完了扱いになる
			return PurchaseProcessingResult.Complete;
		}
	
		//===========================================================
		// 失敗
		//===========================================================
		public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
		{
			// 製品購入が成功しなかった。
			// 詳しい情報はfailureReasonをチェック。
			// ユーザーに失敗の理由はシェアした方がいい
			Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}",product.definition.storeSpecificId, failureReason));
			onBuyFailed(failureReason.ToString());
		}
	}
}
#else
namespace Client.Framework.Ad
{

	// Dammy 定義されてないのでここで仮定義する
	/*
	public class Product
	{
		public string isoCurrencyCode; 		// 通貨のコード。JPYが取れました。
		public string localizedDescription; // 課金アイテムの説明に書いた文
		public string localizedPrice; 		// 設定した金額のdecimal形式
		public string localizedPriceString; // 設定した金額のstring形式（「\120」みたいな感じで取れます）
		public string localizedTitle; 		// 課金アイテムのタイトルに書いた文（Android　「タイトルに書いた文 （アプリ名）」　みたいな形式になります）
	}
*/
	// Dammy
	public class Purchaser : MonoBehaviour
	{
		public void Initialize(
			List<string> nonConsumableItemList,
			List<string> consumableItemList,
//			Action<Product[]> onSuccess,
			Action<MonoBehaviour[]> onSuccess,
			Action<string> onFailed) 
		{
			onFailed("");
		}
	
		public bool IsInitialized(){return true;}

		public bool BuyProductID(string productId, Action<string> onSuccess, Action<string> onFailed)
		{
			return false;
		}

		public void RestorePurchases(){}
	}
}
#endif