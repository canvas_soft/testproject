//==============================================================
// Debug.cs
// Copyright (c) CanvasSoft Co.Ltd. All rights reserved.
//==============================================================
/**
	@file   Debug.cs
	@brief  Debugのオーバーライド.本番でLogを出力しない
	        全てのDebugをオーバーライドするのでnamespaceは無し
	@author Amano Kenta
*/
//==============================================================
// include
//==============================================================
using UnityEngine;


//==============================================================
// NOTE
// Editer上でトレース位置が特定できないため本番のLog吐き禁止用にのみ利用する
// 
// TODO
// 処理速度を考慮すると[Conditional("UNITY_EDITOR")]を使った方がよいが
// 実機でもDebugを吐きたい時があるのでEDITOR or isDebug時のみにしたい
//==============================================================
//#if !UNITY_EDITOR
public static class Debug
{
	// original
	static public void Assert(bool flag, string message = "Assert Failed!!")
	{
		if (!isDebugBuild)return;
		if (flag)return;
		LogError(message);
		Break();
	}

	static public void Assert(bool flag, object message)
	{
		if (flag)return;
		LogError(message);
		Break();
	}

	static public void Pause()
	{
		if (!isDebugBuild)return;
		#if UNITY_EDITOR
		if (UnityEditor.EditorApplication.isPlaying) {
			UnityEditor.EditorApplication.isPaused = true;
		}
		#endif
	}

	// default
    static public void Break()
    {
		if (!isDebugBuild)return;
		UnityEngine.Debug.Break();
    }

    static public void Log(object message)
    {
		if (!isDebugBuild)return;
		UnityEngine.Debug.Log(message);
    }

    static public void Log(object message, Object context)
    {
		if (!isDebugBuild)return;
		UnityEngine.Debug.Log(message, context);
    }

	static public void AssertFormat(bool condition, string format, params object[] args)
	{
		if (!isDebugBuild)return;
		UnityEngine.Debug.AssertFormat(condition, format, args);
	}

    static public void LogError(object message)
    {
		if (!isDebugBuild)return;
		UnityEngine.Debug.LogError(message);
    }

    static public void LogError(object message, Object context)
    {
		if (!isDebugBuild)return;
		UnityEngine.Debug.LogError(message, context);
    }

	static public void LogErrorFormat(string format, params object[] args)
	{
		if (!isDebugBuild)return;
		UnityEngine.Debug.LogErrorFormat(format, args);
	}

    static public void LogWarning(object message)
    {
		if (!isDebugBuild)return;
		UnityEngine.Debug.LogWarning(message);
    }

    static public void LogWarning(object message, Object context)
    {
		if (!isDebugBuild)return;
		UnityEngine.Debug.LogWarning(message, context);
    }

	static public void LogWarningFormat(string format, params object[] args)
	{
		if (!isDebugBuild)return;
		UnityEngine.Debug.LogWarningFormat(format, args);
	}

	static public void LogAssertionFormat(string format, params object[] args)
	{
		if (!isDebugBuild)return;
		UnityEngine.Debug.LogAssertionFormat(format, args);
	}

	public static void LogFormat(string format, params object[] args)
	{
		if (!isDebugBuild)return;
		UnityEngine.Debug.LogFormat(format, args);	
	}
	
    static public void DrawLine(Vector3 v1, Vector3 v2, Color c)
    {
		if (!isDebugBuild)return;
		UnityEngine.Debug.DrawLine(v1, v2, c);
    }

	static public void WriteLine(object message)
	{
		if (!isDebugBuild)return;
		UnityEngine.Debug.Log(message);
	}

	static public bool isDebugBuild 
	{
		get { return true; }//UnityEngine.Debug.isDebugBuild; }
	}
}  
//#endif