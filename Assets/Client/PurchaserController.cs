#define USE_PURCHASE
using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;

using Client.Framework.Purchases;

using System.Linq;

#if USE_PURCHASE
using UnityEngine.Purchasing;
#endif


namespace Client.Game.Controller
{
	// シーンに戻るたびに？？？
	public class PurchaserController : MonoBehaviour
	{
		// 購入後の処理はここで記述
		public Action<string> onBuySuccess = delegate {};

		private enum SaveKey
		{
			PurchaserBuyCount,
		}

		//-----------------------------------------------------------
		// 商品を登録
		// 個人開発なので直書き
		//-----------------------------------------------------------
		// 非消費型商品一覧
		public static readonly List<string> NonConsumableItemList = new List<string>
		{
			"com.canvassoft.test.removead",
		};

		// 消費型商品一覧
		// 登録順に結果も帰って来る
		public static readonly List<string> ConsumableItemList = new List<string> 
		{
			"com.canvassoft.test.item0", // 
			"com.canvassoft.test.item1", // 
		};
			
		public const string RestoreKey = "com.canvassoft.test.restore";
		public const string RestoreSuccessKey = "com.canvassoft.test.restore.success";
		public const string RestoreFailedKey = "com.canvassoft.test.restore.failed";

		//-----------------------------------------------------------
		// 他の表示用の商品情報
		//-----------------------------------------------------------
		public class ItemData
		{
			public string key;

			public int productId;		// ProductMasterのProductId
			public string prefabName;	// プレハブ名
			public int buyableCount;	// 購入できる回数(0は無限)

			// public bool once;	// 一度きりか?
			// public string month; // 月一のとか??? 将来的に

			// 追加する数(今回はマスターに頼らないためここに記述)
			public int count;
			public string title;
			public string comment;

			public ItemData(string key, int productId, string prefabName, int buyableCount, int count, string title, string comment)
			{
				this.key = key;
				this.productId = productId;
				this.prefabName = prefabName;
				this.buyableCount = buyableCount;
				this.count = count;
				this.title = title;
				this.comment = comment;
			}
		};

		private static readonly List<ItemData> itemList = new List<ItemData>
		{
			new ItemData("com.canvassoft.test.removead", 100, "PurchaseRemoveAdButton", 1, 0, "PURCHASE_TITLE_REMOVE_AD", "PURCHASE_COMMENT_REMOVE_AD"),
			new ItemData("com.canvassoft.test.item0", 201, "PurchaseFastItemButton", 0, 10, "PURCHASE_TITLE_ITEM0", "PURCHASE_COMMENT_ITEM0"),
			new ItemData("com.canvassoft.test.item1", 202, "PurchaseFastItemButton", 0, 35, "PURCHASE_TITLE_ITEM1", "PURCHASE_COMMENT_ITEM1"),
			new ItemData("com.canvassoft.test.item2", 203, "PurchaseFastItemButton", 0, 65, "PURCHASE_TITLE_ITEM2", "PURCHASE_COMMENT_ITEM2"),
			new ItemData("com.canvassoft.test.item3", 204, "PurchaseFastItemButton", 0, 130, "PURCHASE_TITLE_ITEM3", "PURCHASE_COMMENT_ITEM3"),
		};

		public ItemData GetItemData(string key)
		{
			return itemList.FirstOrDefault(item => item.key == key);
		}

		public void OnInit()
		{
			ShowItemDialog(()=>{
				Debug.Log("Initialize Finish");
			});
		}

		public void OnClick()
		{
			Debug.Log ("OnClick");
			Buy ("com.canvassoft.test.item0", (result) => {
				Debug.Log("Result:"+result);
			});
		}
			
		//-----------------------------------------------------------
		// 
		//-----------------------------------------------------------
		private Purchaser _purchaser;
		private Purchaser purchaser
		{
			get {
				if(_purchaser == null) {
					// GameObjectを作成してControllerの下にぶら下げる
					GameObject obj = new GameObject("Purchaser");
					obj.transform.SetParent(this.transform);
					_purchaser = obj.AddComponent<Purchaser>();
				}
				return _purchaser;
			}
		}

		private Action onClosed = delegate {};

		//======================================================
		// 購入ダイアログ表示
		//======================================================
		public void ShowItemDialog(Action onComplete)
		{
			
			onClosed = onComplete;

			if(purchaser.IsInitialized())
			{
				Debug.Log("ShowItemDialog : LoadingShowItemDialog");
			}
			else
			{
				// ログイン完了次第ダイアログを表示
				#if USE_PURCHASE
				Debug.Log("ShowItemDialog : purchaser.Initalize");

				purchaser.Initialize(
					NonConsumableItemList,
					ConsumableItemList,
					InializeSuccess, 
					OnInitializeFailed);
				#endif
			}
		}

		// 初期時にデータを追加
		private void InializeSuccess()
		{
			#if USE_PURCHASE
			// データを補完する
			for(int i = 0; i < purchaser.productDataList.Count; i++)
			{
				ItemData item = GetItemData(purchaser.productDataList[i].key);

				purchaser.productDataList[i].productId = item.productId;
				purchaser.productDataList[i].prefabName = item.prefabName;
				purchaser.productDataList[i].buyableCount = item.buyableCount;	// 購入可能回数

				string countKey = SaveKey.PurchaserBuyCount.ToString() + item.productId;
				purchaser.productDataList[i].buyedCount = 0;//PlayerPrefsEx.GetInt(countKey);	// 購入履歴数
			}
				
			// データ設定後、そのままダイアログを出す
			onClosed();
			#endif
		}

		// ログインが確定している状態なので購入ダイアログを表示
		private void LoginedShowItemDialog()
		{
			#if USE_PURCHASE
//			// 値段情報をセット
//			dialog.SetProducts(purchaser.productDataList, () => 
//			{
//				// ローディング終了
//				ShowLoading(false);
//
//				dialog.Show(
//						// 購入商品が選択された
//						(string key) => 
//						{
//							// ボタンindexからpurchase Keyを取得してControllerに流す
//							Buy(key,
//								(string result) => 
//								{
//									// とりあえずエラーの時とかのフローとかあるので
//									// 成功しようが、失敗しようがボタン押した瞬間にダイアログ消しちゃう
//									onClosed();
//								});
//						},
//						// 購入がキャンセルされた
//						() =>
//						{
//							onClosed();
//						});
//			});
			#endif
		}

		// ログインに失敗したのでエラーダイアログを出して終わる
		private void OnInitializeFailed(string error)
		{

			string comment = "DIALOG_PURCHASE_ERROR";
			switch(error)
			{
			case "UserCancelled":
				comment += "1";
				break;

			case "PurchasingUnavailable":
				comment += "2";
				break;

			case "NoProductsAvailable":
				comment += "3";
				break;

			case "AppNotKnown":
				comment += "4";
				break;

			default:break;
			}

			Debug.Log (comment);

			// TODO:エラー表示が出たのでメニューに戻るとかの処理をかく
			onClosed();

		}

		//======================================================
		// 選択して実際に商品を購入
		//======================================================
		public void Buy(string itemKey, Action<string> onSuccess)
		{
			#if USE_PURCHASE
			// リストア
			if (itemKey == RestoreKey)
			{
				purchaser.RestorePurchases ((result) =>
				{
					onBuySuccess(result ? RestoreSuccessKey : RestoreFailedKey);

					onClosed ();
				});
			}
			else
			{
				if (!purchaser.BuyProductID (itemKey, OnBuySuccess, OnBuyFailed))
				{
					Debug.Log ("エラーが発生したため、購入をキャンセルします");

					onClosed ();
				}
			}
			#endif
		}

		private void OnBuySuccess(string result)
		{
			ItemData data = GetItemData(result);

			// ここで購入付与を行う
			Debug.Log("購入完了:" + result);
			onBuySuccess(result);

			// TODO:一回限りの処理を暫定的に書く
			string countKey = SaveKey.PurchaserBuyCount.ToString() + data.productId;

			onClosed();
		}

		private void OnBuyFailed(string error)
		{
			string title = "";
			string message = "";//"DIALOG_PURCHASE_ERROR";
			switch(error)
			{
			case "PurchasingUnavailable":
				message = "システムの購入機能が利用できません。購入をキャンセルします。";
				//message += "5";
				break;
			case "ExistingPurchasePending":
				message = "新たに購入をリクエストしましたが、すでに購入処理中でした。";
				//message += "6";
				break;
			case "ProductUnavailable":
				message = "ストアで購入できる商品ではありません。購入をキャンセルします。";
				//message += "7";
				break;
			case "SignatureInvalid":
				message = "課金レシートの検証に失敗しました。購入をキャンセルします。";
				//message += "8";
				break;
			case "UserCancelled":
				message = "購入がキャンセルされました。";
				//message += "9";
				break;
			case "PaymentDeclined":
				message = "支払いに問題がありました。購入をキャンセルします。";
				//message += "10";
				break;
			case "Unknown":
			default:
				message = "エラーが発生しました。購入をキャンセルします。";
				//message += "11";
				break;
			}
			Debug.Log("OnBuyFailed: " + message);
			onClosed();
		}
	}
}
