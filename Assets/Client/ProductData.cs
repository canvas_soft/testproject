﻿using UnityEngine;
using System.Collections;

namespace Client.Framework.Purchases
{
	// 課金クラスProductにゲームごとのマスターデータの情報を追加した拡張クラス
	public class ProductData
	{
		public string key;
		public string isoCurrencyCode; 		// 通貨のコード。JPYが取れました。
		public string localizedDescription; // 課金アイテムの説明に書いた文
		public string localizedPrice; 		// 設定した金額のdecimal形式
		public string localizedPriceString; // 設定した金額のstring形式（「\120」みたいな感じで取れます）
		public string localizedTitle; 		// 課金アイテムのタイトルに書いた文（Android　「タイトルに書いた文 （アプリ名）」　みたいな形式になります）

		// 追加情報
		public int productId;
		public string prefabName;
		public int buyableCount;	// 購入可能回数
		public int buyedCount;		// 購入履歴数

		public ProductData(
			string key,
			string isoCurrencyCode,
			string localizedDescription,
			string localizedPrice,
			string localizedPriceString,
			string localizedTitle)
		{
			this.key = key;

			this.isoCurrencyCode = isoCurrencyCode;
			this.localizedDescription = localizedDescription;
			this.localizedPrice = localizedPrice;
			this.localizedPriceString = localizedPriceString;
			this.localizedTitle = localizedTitle;
		}
	}
}
